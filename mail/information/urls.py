from rest_framework.routers import DefaultRouter
from .views import WomenViewSet
from django.urls import path


# urlpatterns = [
#     path('information/', WomenViewSet.as_view({'get': 'list'})),
#     path('information/<int:pk>', WomenViewSet.as_view({'get': 'retrive'})),
# ]

router = DefaultRouter()
router.register(r'information', WomenViewSet, basename='info')

urlpatterns = router.urls
