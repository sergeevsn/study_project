from django.contrib import admin
from .models import Mail, Hair, Women

# Register your models here.
admin.site.register(Mail)
admin.site.register(Hair)
admin.site.register(Women)
