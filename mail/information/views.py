from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework import viewsets
from django.shortcuts import get_object_or_404

# Create your views here.


class WomenView(APIView):
    def get(self, request):
        womens = Women.objects.all()
        serializer = WomenSerializer(womens, many=True)
        return Response({"womens": serializer.data})

    def post(self, request):
        women = request.data.get('womens')
        serializer = WomenSerializer(data=women)
        if serializer.is_valid(raise_exception = True):
            womens_saved = serializer.save()
        return Response({"ok": "vse ok"})



class WomenViewSet(viewsets.ModelViewSet):
    serializer_class = WomenSerializer

    queryset = Women.objects.all()
