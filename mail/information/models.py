from django.db import models
from django.utils import timezone

# Create your models here.

class Mail(models.Model):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=100
        )
    text = models.TextField()
    created_date = models.DateTimeField(
        default=timezone.now,
        null=True,
        blank=True,
        )
    text2 = models.TextField()

    def __str__(self):
        return self.title


class Women(models.Model):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=100
        )
    content = models.TextField()
    time_create = models.DateTimeField(
        default=timezone.now,
        null=True,
        blank=True,
        )
    is_student = models.BooleanField(default=False)
    hair = models.ForeignKey('Hair', on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_hair_length(self):
        return str(self.hair.length)



class Hair(models.Model):
    color = models.CharField(
        verbose_name='Цвет',
        max_length=100
        )
    length = models.IntegerField()

    def __str__(self):
        return self.color
